from logging import Logger

from redis.asyncio import ConnectionPool
from redis.asyncio.client import Redis
from redis.exceptions import RedisError
from aiormq import AMQPConnectionError
from sqlalchemy.ext.asyncio import AsyncEngine
from faststream.rabbit import RabbitBroker, RabbitQueue, RabbitExchange, ExchangeType

from backend.usecases.protocols.connection_check_proto import ConnectionCheckProtocol


class ConnectionCheckAdapter(ConnectionCheckProtocol):

    def __init__(self, logger: Logger, engine: AsyncEngine, broker: RabbitBroker, cache_pool: ConnectionPool):
        self.logger = logger
        self.engine = engine
        self.broker = broker
        self.cache_pool = cache_pool

    async def _check_db_connection(self) -> bool:
        try:
            self.logger.info("Connecting to Postgres...")
            async with self.engine.connect():
                self.logger.info(f"Connected to Postgres database")
            return True
        except ConnectionRefusedError as error:
            self.logger.critical("Database is not reachable!")
            self.logger.critical(f"{error}")
            return False
        except OSError as error:
            self.logger.critical("Database is not reachable!")
            self.logger.critical(f"{error}")
            return False

    async def _check_broker_connection(self) -> bool:
        self.logger.info("Connecting to RabbitMQ...")
        try:
            await self.broker.connect()
            await self.broker.declare_exchange(RabbitExchange(
                    name='users',
                    type=ExchangeType.DIRECT,
                ))
            await self.broker.declare_queue(RabbitQueue(name='user.registered', routing_key='user.registered'))
            self.logger.info("Connected to RabbitMQ")
            return True
        except AMQPConnectionError as error:
            self.logger.critical("RabbitMQ is not reachable!")
            self.logger.critical(f"{error}")
            return False

    async def _check_cache_connection(self) -> bool:
        self.logger.info("Connecting to Redis...")
        try:
            redis = Redis.from_pool(connection_pool=self.cache_pool)
            await redis.ping()
            self.logger.info("Connected to Redis")
            await redis.aclose()
            return True
        except RedisError as error:
            self.logger.critical("Redis is not reachable!")
            self.logger.critical(f"{error}")
            return False

    async def __call__(self) -> bool:
        connections = (await self._check_db_connection(),
                       await self._check_broker_connection(),
                       await self._check_cache_connection())
        return all(connections)
