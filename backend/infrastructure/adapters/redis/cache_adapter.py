from redis.asyncio.client import Redis
from redis.exceptions import RedisError
from logging import Logger

from backend.usecases.protocols.cache_proto import CacheProtocol


class RedisCacheAdapter(CacheProtocol):
    def __init__(self, client: Redis, logger: Logger):
        self.redis = client
        self.logger = logger

    async def get(self, name: str) -> str:
        try:
            value = await self.redis.get(name)
            return value
        except RedisError as error:
            self.logger.error(f"{error}")

    async def set(self, name: str, value: str, *args, **kwargs) -> bool:
        try:
            await self.redis.set(name, value, *args, **kwargs)
            return True
        except RedisError as error:
            self.logger.error(f"{error}")

    async def get_del(self, name: str) -> str:
        try:
            value = await self.redis.getdel(name)
            return value
        except RedisError as error:
            self.logger.error(f"{error}")
