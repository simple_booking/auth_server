from typing import Any
from faststream.rabbit import RabbitBroker
from backend.usecases.protocols.broker_proto import BrokerProtocol


class MQBrokerAdapter(BrokerProtocol):

    def __init__(self, broker_client: RabbitBroker):
        self.client = broker_client

    async def publish_message(self, message: dict[str, Any], exchange: str, routing_key: str):
        await self.client.publish(message=message, exchange=exchange, routing_key=routing_key,
                                )