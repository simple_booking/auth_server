from logging import Logger
from typing import Union
from sqlalchemy import insert, update, select
from sqlalchemy.exc import SQLAlchemyError
from pydantic import ValidationError
from sqlalchemy.ext.asyncio import AsyncSession

from backend.usecases.dto.users import UserWithHashDTO
from backend.usecases.protocols.user_data_proto import UserDataAccessProtocol
from backend.entities.models.user import User


class SQLAlchemyUserAdapter(UserDataAccessProtocol):

    def __init__(self, model: User, async_session: AsyncSession, logger: Logger):
        self.model = model
        self.session = async_session
        self.logger = logger

    async def create_user(self, user_data: UserWithHashDTO) -> bool:
        query = insert(self.model).values(email=user_data.email,
                                          hashed_password=user_data.hashed_password)
        try:
            await self.session.execute(query)
            await self.session.commit()
            return True
        except SQLAlchemyError as error:
            self.logger.error(error)
            return False

    async def activate_user(self, user_email: str) -> bool:
        query = update(self.model).where(self.model.email == user_email).values(is_active=True)
        try:
            await self.session.execute(query)
            await self.session.commit()
            return True
        except SQLAlchemyError as error:
            self.logger.error(error)
            return False

    async def get_by_email(self, email: str) -> Union[bool, UserWithHashDTO]:
        query = select(self.model).where(self.model.email == email)
        try:
            result = await self.session.execute(query)
            user = result.scalar()
            dto = UserWithHashDTO.from_orm(user)
            return dto
        except SQLAlchemyError as error:
            self.logger.error(error)
            return False
        except ValidationError as error:
            self.logger.error(error)
            return False
