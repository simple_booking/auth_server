from logging import Logger
from sqlalchemy import insert
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession

from backend.entities.models.clients import Clients
from backend.usecases.dto.clients import ClientCreateWithHash
from backend.usecases.protocols.client_data_proto import ClientDataAccessProtocol


class SQLAlchemyClientAdapter(ClientDataAccessProtocol):

    def __init__(self, model: Clients, async_session: AsyncSession, logger: Logger):
        self.model = model
        self.session = async_session
        self.logger = logger

    async def create_client(self, data: ClientCreateWithHash) -> bool:
        try:
            query = insert(self.model).values(app_name=data.app_name, client_id=data.client_id,
                                              client_secret=data.client_secret, created_at=data.created_at)
            await self.session.execute(query)
            await self.session.commit()
            return True
        except IntegrityError:
            return False
