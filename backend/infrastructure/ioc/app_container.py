import logging
from asyncio import Lock
from dishka import make_async_container, AsyncContainer
from fastapi import FastAPI
from dishka.integrations.fastapi import setup_dishka as setup_fastapi_ioc
from faststream.rabbit.fastapi import RabbitRouter

from backend.infrastructure.ioc.service_provider import create_providers
from backend.infrastructure.logger.logger import logger

container = make_async_container(create_providers(),
                                 context={logging.Logger: logger},
                                 lock_factory=Lock)


def get_ioc_container() -> AsyncContainer:
    return container


def setup_ioc(app: FastAPI, container: AsyncContainer = get_ioc_container()):
    setup_fastapi_ioc(container, app)
    logger.info("IoC container created")
