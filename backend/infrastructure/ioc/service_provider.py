from typing import AsyncIterable
from logging import Logger, INFO
from redis.asyncio import ConnectionPool
from redis.asyncio.client import Redis
from dishka import provide, Provider, Scope, from_context
from faststream.rabbit import RabbitBroker
from sqlalchemy.ext.asyncio import async_sessionmaker, AsyncEngine, create_async_engine, AsyncSession

from backend.main.config import Settings
from backend.entities.models.user import User
from backend.entities.models.clients import Clients
from backend.usecases.user import UserUseCases
from backend.usecases.client import ClientUseCases
from backend.infrastructure.adapters.redis.cache_adapter import RedisCacheAdapter
from backend.infrastructure.adapters.sqlalchemy_adapters.user_adapter import SQLAlchemyUserAdapter
from backend.infrastructure.adapters.sqlalchemy_adapters.client_adapter import SQLAlchemyClientAdapter
from backend.infrastructure.adapters.connection_check.connection_check_adapter import ConnectionCheckAdapter


class MainProvider(Provider):
    logger = from_context(provides=Logger, scope=Scope.APP)
    cache_adapter = provide(source=RedisCacheAdapter, scope=Scope.REQUEST)
    user_dao = provide(source=SQLAlchemyUserAdapter, scope=Scope.REQUEST)
    client_dao = provide(source=SQLAlchemyClientAdapter, scope=Scope.REQUEST)
    connection_check = provide(source=ConnectionCheckAdapter, scope=Scope.APP)

    @provide(scope=Scope.APP)
    def provide_configs(self) -> Settings:
        return Settings()

    @provide(scope=Scope.APP)
    async def provide_redis_pool(self, settings: Settings) -> AsyncIterable[ConnectionPool]:
        connection_pool = ConnectionPool.from_url(url=settings.redis.REDIS_URL)
        yield connection_pool
        await connection_pool.aclose()

    @provide(scope=Scope.REQUEST)
    async def provide_redis_client(self, connection_pool: ConnectionPool) -> AsyncIterable[Redis]:
        client = Redis(connection_pool=connection_pool, decode_responses=True)
        yield client
        await client.aclose()

    @provide(scope=Scope.APP)
    async def provide_db_engine(self, settings: Settings, logger: Logger) -> AsyncIterable[AsyncEngine]:
        engine = create_async_engine(url=settings.db.DB_URL)
        yield engine
        logger.info("Disconnecting from Postgres")
        await engine.dispose()
        logger.info("Postgres connection closed")

    @provide(scope=Scope.REQUEST)
    async def provide_db_session(self, engine: AsyncEngine) -> AsyncIterable[AsyncSession]:
        async with async_sessionmaker(engine)() as session:
            yield session

    @provide(scope=Scope.APP)
    async def provide_mq_broker(self, settings: Settings, logger: Logger) -> AsyncIterable[RabbitBroker]:
        broker = RabbitBroker(settings.broker.RABBITMQ_URL,
                              logger=logger,
                              log_level=INFO,
                              app_id="auth_server")
        yield broker
        logger.info("Closing broker connection...")
        await broker.close()
        logger.info("Broker connection closed")

    @provide(scope=Scope.REQUEST)
    async def provide_user_model(self) -> User:
        return User

    @provide(scope=Scope.REQUEST)
    async def provide_client_model(self) -> Clients:
        return Clients

    @provide(scope=Scope.REQUEST)
    async def provide_user_use_cases(self, dao: SQLAlchemyUserAdapter, cache: RedisCacheAdapter,
                                     broker: RabbitBroker) -> UserUseCases:
        return UserUseCases(dao=dao, cache=cache, broker=broker)

    @provide(scope=Scope.REQUEST)
    async def provide_client_use_cases(self, dao: SQLAlchemyClientAdapter) -> ClientUseCases:
        return ClientUseCases(dao=dao)


def create_providers() -> MainProvider:
    return MainProvider()
