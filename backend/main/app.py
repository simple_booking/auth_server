from fastapi import FastAPI
from contextlib import asynccontextmanager
from logging import Logger

from backend.infrastructure.ioc.app_container import setup_ioc, get_ioc_container
from backend.infrastructure.adapters.connection_check.connection_check_adapter import ConnectionCheckAdapter
from backend.api.api_v1.api_v1 import api_router as v1_router


@asynccontextmanager
async def lifespan(app: FastAPI):
    connection_check, logger = (await get_ioc_container().get(ConnectionCheckAdapter),
                                await get_ioc_container().get(Logger))
    if not await connection_check():
        logger.critical("Connection checking failed")
    else:
        logger.info("Successfully connected to services")
    yield


app = FastAPI(lifespan=lifespan)
setup_ioc(app)
app.include_router(v1_router)
