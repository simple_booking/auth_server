from pathlib import Path

from pydantic_settings import BaseSettings, SettingsConfigDict

current_path = Path(__file__).resolve()
root_path = current_path.parents[2]


class DataBaseSettings(BaseSettings):
    DB_USER: str
    DB_PASS: str
    DB_HOST: str
    DB_PORT: int
    DB_NAME: str

    @property
    def DB_URL(self):
        return f"postgresql+asyncpg://{self.DB_USER}:{self.DB_PASS}@{self.DB_HOST}:{self.DB_PORT}/{self.DB_NAME}"

    model_config = SettingsConfigDict(env_file=root_path / ".env", extra='ignore')


class BrokerBaseSettings(BaseSettings):
    RABBITMQ_DEFAULT_USER: str
    RABBITMQ_DEFAULT_PASS: str
    RABBITMQ_PORT: int

    @property
    def RABBITMQ_URL(self):
        return f"amqp://{self.RABBITMQ_DEFAULT_USER}:{self.RABBITMQ_DEFAULT_PASS}@rabbitmq:{self.RABBITMQ_PORT}"

    model_config = SettingsConfigDict(env_file=root_path / ".env", extra='ignore')


class RedisBaseSettings(BaseSettings):
    REDIS_PORT: int
    REDIS_USER: str
    REDIS_USER_PASSWORD: str
    REDIS_HOST: str

    @property
    def REDIS_URL(self):
        return f"redis://:{self.REDIS_USER_PASSWORD}@{self.REDIS_HOST}:{self.REDIS_PORT}"

    model_config = SettingsConfigDict(env_file=root_path / ".env", extra='ignore')


class AuthJWT(BaseSettings):
    private_key_path: Path = root_path / "certs" / "jwt-private.pem"
    public_key_path: Path = root_path / "certs" / "jwt-public.pem"
    algorithm: str = "RS256"
    access_expire: int = 1
    refresh_expire: int = 15


class Settings(BaseSettings):
    db: DataBaseSettings = DataBaseSettings()
    broker: BrokerBaseSettings = BrokerBaseSettings()
    redis: RedisBaseSettings = RedisBaseSettings()
    jwt: AuthJWT = AuthJWT()

    model_config = SettingsConfigDict(env_file=root_path / ".env", extra='ignore')


settings = Settings()


def get_settings() -> Settings:
    return settings
