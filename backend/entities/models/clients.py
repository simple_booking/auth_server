from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy import String, Date, LargeBinary
from datetime import date

from backend.entities.models.base_model import Base


class Clients(Base):

    __tablename__ = "clients"

    id: Mapped[int] = mapped_column(primary_key=True, nullable=False)
    app_name: Mapped[str] = mapped_column(String(16), nullable=False, unique=True)
    client_id: Mapped[str] = mapped_column(String(64), nullable=False)
    client_secret: Mapped[str] = mapped_column(LargeBinary(), nullable=False)
    created_at: Mapped[date] = mapped_column(Date(), nullable=False)
