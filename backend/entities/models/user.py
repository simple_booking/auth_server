from sqlalchemy import String, LargeBinary
from sqlalchemy.orm import Mapped, mapped_column
from datetime import date
from backend.entities.models.base_model import Base


class User(Base):
    __tablename__ = "users"

    id: Mapped[int] = mapped_column(primary_key=True, nullable=False)
    first_name: Mapped[str] = mapped_column(String(32), nullable=True)
    last_name: Mapped[str] = mapped_column(String(32), nullable=True)
    email: Mapped[str] = mapped_column(String(64), nullable=False, unique=True)
    date_of_birth: Mapped[date] = mapped_column(nullable=True)
    role: Mapped[str] = mapped_column(default="customer")
    is_active: Mapped[bool] = mapped_column(default=False)
    country: Mapped[str] = mapped_column(nullable=True)
    language: Mapped[str] = mapped_column(default="en")
    hashed_password: Mapped[bytes] = mapped_column(LargeBinary(), nullable=False)