from typing import Union

from faststream.rabbit import RabbitBroker
from fastapi.responses import ORJSONResponse
from secrets import token_urlsafe, randbelow
from datetime import datetime, timedelta, timezone

from backend.usecases.dto.users import UserCreateRequestDTO, UserWithHashDTO, UserLoginForm, UserOTP
from backend.usecases.dto.queue import CodeDTO
from backend.usecases.security.password_utils import hash_password, validate_password
from backend.usecases.security.jwt_utils import encode_HS256_token
from backend.usecases.protocols.cache_proto import CacheProtocol
from backend.usecases.protocols.user_data_proto import UserDataAccessProtocol
from backend.usecases.protocols.broker_proto import BrokerProtocol


class UserUseCases:

    def __init__(self, dao: UserDataAccessProtocol, broker: BrokerProtocol, cache: CacheProtocol):
        self.dao = dao
        self.broker = broker
        self.cache = cache

    async def create_user(self, data: UserCreateRequestDTO) -> bool:
        result = await self.dao.create_user(UserWithHashDTO(email=data.email,
                                                            hashed_password=hash_password(data.password)
                                                            ))
        if not result:
            return False
        message = CodeDTO(email=data.email, otp=token_urlsafe(16),
                          exp=datetime.now(timezone.utc) + timedelta(minutes=60),
                          title='user_created')
        await self.cache.set(name=data.email + "_activation", value=message.otp, ex=3600)
        await self.broker.publish_message(message=message.model_dump(), exchange="users",
                                          routing_key="user.registered")

        return result

    async def activate_user(self, data: UserOTP) -> bool:
        saved_code = await self.cache.get_del(data.email + "_activation")
        if not saved_code:
            return False
        if saved_code != data.otp:
            return False
        return await self.dao.activate_user(user_email=data.email)

    async def login(self, data: UserLoginForm) -> bool:
        result = await self.dao.get_by_email(data.email)
        if not result:
            return False
        valid_pass = validate_password(data.password, result.hashed_password)
        if not valid_pass:
            return False
        otp = randbelow(10000)
        message = CodeDTO(email=data.email, otp=otp,
                          exp=datetime.now(timezone.utc) + timedelta(minutes=1),
                          title='user_2fa')
        await self.cache.set(name=data.email + "_2fa", value=otp, ex=60)
        await self.broker.publish(message=message.model_dump(), exchange="users", routing_key="user.2fa")
        return True
