from typing import Protocol, runtime_checkable

@runtime_checkable
class ConnectionCheckProtocol(Protocol):

    async def _check_db_connection(self) -> bool: ...

    async def _check_cache_connection(self) -> bool: ...

    async def _check_broker_connection(self) -> bool: ...
