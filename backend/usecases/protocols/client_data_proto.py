from typing import Protocol, runtime_checkable

from backend.usecases.dto.clients import ClientCreateWithHash


class ClientDataAccessProtocol(Protocol):
    async def create_client(self, data: ClientCreateWithHash) -> bool: ...
