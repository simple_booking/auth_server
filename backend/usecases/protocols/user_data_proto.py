from typing import Protocol, Union

from backend.usecases.dto.users import UserWithHashDTO


class UserDataAccessProtocol(Protocol):

    async def create_user(self, user_data: UserWithHashDTO) -> bool: ...

    async def activate_user(self, user_email: str) -> bool: ...

    async def get_by_email(self, email: str) -> Union[bool, UserWithHashDTO]: ...
