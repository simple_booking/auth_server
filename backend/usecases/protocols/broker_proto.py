from typing import Protocol, Any


class BrokerProtocol(Protocol):
    async def publish_message(self, message: dict[str, Any], exchange: str, routing_key: str): ...
