from typing import Union
from secrets import token_urlsafe

from backend.usecases.protocols.client_data_proto import ClientDataAccessProtocol
from backend.usecases.dto.clients import ClientCreateRequestDTO, ClientCreateWithHash
from backend.usecases.security.password_utils import hash_password


class ClientUseCases:

    def __init__(self, dao: ClientDataAccessProtocol):
        self.dao = dao

    async def create_client(self, data: ClientCreateRequestDTO) -> Union[bool, str]:
        client_secret = token_urlsafe(16)
        client = ClientCreateWithHash(app_name=data.app_name, client_secret=hash_password(client_secret))
        result = await self.dao.create_client(client)
        if not result:
            return False
        return client_secret
