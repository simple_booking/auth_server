from pydantic import BaseModel, EmailStr
from typing import Union
from datetime import datetime


class CodeDTO(BaseModel):
    email: EmailStr
    otp: Union[str, int]
    exp: datetime
    title: str
