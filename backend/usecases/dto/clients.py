from pydantic import BaseModel, StrictBytes
from typing import Annotated
from uuid import uuid4

from datetime import datetime, timezone


class ClientCreateRequestDTO(BaseModel):
    app_name: Annotated[str, str]


class ClientCreateWithHash(ClientCreateRequestDTO):
    client_id: Annotated[str, str] = uuid4().__str__()
    client_secret: Annotated[StrictBytes, StrictBytes]
    created_at: Annotated[datetime, datetime] = datetime.now(timezone.utc)
