from pydantic import BaseModel, field_validator, EmailStr, StrictBytes, ValidationError
from pydantic_core import InitErrorDetails
from typing import Annotated, Union
import re


class UserBaseDTO(BaseModel):
    email: Annotated[str, EmailStr]

    class Config:
        from_attributes = True


class UserCreateRequestDTO(UserBaseDTO):
    """
    Data class is used for validating data from request and transferring it to service class
    """

    password: Annotated[str, str]

    @field_validator("password")
    def validate_password(cls, value):
        regex = r"(?=.*[0-9]?)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{6,}"
        if len(value) < 8:
            msg = "password must be at least 8 characters long"
            raise ValidationError.from_exception_data(title='error',
                                                      line_errors=[InitErrorDetails(type='value_error',
                                                                                    loc=('password',),
                                                                                    input=value,
                                                                                    ctx={'error': msg})])
        if not re.search(regex, value):
            msg = "password must contain uppercase and lowercase letters and at least 1 special character"
            raise ValidationError.from_exception_data(title='error',
                                                      line_errors=[InitErrorDetails(type='value_error',
                                                                                    loc=('password',),
                                                                                    input=value,
                                                                                    ctx={'error': msg})])
        return value


class UserLoginForm(BaseModel):
    """
    Data class is used for transferring data from request to service class
    """

    email: Annotated[str, EmailStr]
    password: Annotated[str, str]


class UserWithHashDTO(UserBaseDTO):
    """
    Data class is used for validating data and transferring it to repo class
    """

    hashed_password: Annotated[bytes, StrictBytes]


class UserOTP(BaseModel):
    email: Annotated[str, EmailStr]
    otp: Union[str, int]
    client_id: Annotated[str, str] = None
