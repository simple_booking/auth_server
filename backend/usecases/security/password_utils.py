import bcrypt


def hash_password(password: str) -> bytes:
    return bcrypt.hashpw(password=password.encode(), salt=bcrypt.gensalt(6))


def validate_password(password: str, hashed_password: bytes) -> bool:
    return bcrypt.checkpw(password=password.encode(), hashed_password=hashed_password)
