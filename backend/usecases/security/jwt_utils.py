import jwt
from datetime import datetime, timedelta, timezone
from jwt.exceptions import PyJWTError

from backend.main.config import get_settings


def encode_HS256_token(
        user_id: int,
        type: str,
        audience: str,
        secret_key: str ,
        algorithm: str = "HS256",
        exp: str = datetime.now(timezone.utc) + timedelta(minutes=1)
):
    payload = {
        "iss": "auth.s_b",
        "aud": audience,
        "sub": user_id,
        "exp": exp,
        "iat": datetime.now(timezone.utc),
        "type": type
    }
    return jwt.encode(payload=payload, key=secret_key, algorithm=algorithm)


 # private_key: str = get_settings().auth_jwt.private_key_path.read_text(),
 #        algorithm: str = get_settings().auth_jwt.algorithm,
 #        access_expire: int = get_settings().auth_jwt.access_expire,
 #        refresh_expire: int = get_settings().auth_jwt.refresh_expire

# public_key: str = get_settings().auth_jwt.public_key_path.read_text(),
# algorithm: str = get_settings().auth_jwt.algorithm


def encode_jwt(
        user_id: int,
        token_type: str,
        audience: str,
        private_key: str = get_settings().jwt.private_key_path.read_text(),
        access_expire: int = get_settings().jwt.access_expire,
        refresh_expire: int = get_settings().jwt.refresh_expire,
        algorithm: str = get_settings().jwt.algorithm
):
    payload = {
        "iss": "auth.s_b",
        "aud": audience,
        "sub": user_id,
        "type": type
    }
    now = datetime.now(timezone.utc)
    if token_type == "access":
        payload.update(exp=now + timedelta(minutes=access_expire), iat=now)
    else:
        payload.update(exp=now + timedelta(days=refresh_expire), iat=now)
    encoded = jwt.encode(
        payload,
        private_key,
        algorithm
    )
    return encoded


def decode_jwt(
        token: str | bytes,
        public_key: str ,
        algorithm: str
):
    try:
        decoded = jwt.decode(
            token,
            public_key,
            algorithms=[algorithm]
        )
        return decoded
    except PyJWTError:
        return False
