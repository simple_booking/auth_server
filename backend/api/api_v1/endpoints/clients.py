from fastapi import APIRouter, Body, HTTPException, status
from fastapi.responses import ORJSONResponse
from dishka.integrations.fastapi import inject, FromDishka

from backend.usecases.responses.responses import ResponseModel
from backend.usecases.dto.clients import ClientCreateRequestDTO
from backend.usecases.client import ClientUseCases

router = APIRouter(prefix='/clients')
endpoints_tag = ["Clients"]


@router.post('/register',
             tags=endpoints_tag,
             response_class=ORJSONResponse,
             responses={201: {"model": ResponseModel}, 409: {"model": ResponseModel}, 422: {"model": ResponseModel}}
             )
@inject
async def register_client(service: FromDishka[ClientUseCases],
                          data: ClientCreateRequestDTO = Body(example={"app_name": "string"})):
    response = await service.create_client(data)
    if not response:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail="App name already exist")
    return ORJSONResponse(status_code=status.HTTP_200_OK, content={"client_secret": response})
