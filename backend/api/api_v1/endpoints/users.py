from fastapi import APIRouter, HTTPException, status, Form
from fastapi.responses import ORJSONResponse
from typing import Annotated
from pydantic import EmailStr
from dishka.integrations.fastapi import FromDishka, inject

from backend.usecases.dto.users import UserCreateRequestDTO, UserLoginForm, UserOTP
from backend.usecases.user import UserUseCases
from backend.usecases.responses.responses import ResponseModel

router = APIRouter(prefix="/users")
endpoints_tag = ["Users"]


@router.post('/register',
             status_code=status.HTTP_201_CREATED,
             tags=endpoints_tag,
             response_class=ORJSONResponse,
             responses={201: {"model": ResponseModel}, 409: {"model": ResponseModel}, 422: {"model": ResponseModel}})
@inject
async def create_user(email: Annotated[EmailStr, Form()], password: Annotated[str, Form()],
                      service: FromDishka[UserUseCases]):
    input_data = UserCreateRequestDTO(email=email, password=password)
    response = await service.create_user(input_data)
    if not response:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail="email already used!")
    return ORJSONResponse(status_code=status.HTTP_201_CREATED, content={"message": "ok"})


@router.get('/register/activate',
            status_code=status.HTTP_200_OK,
            tags=endpoints_tag,
            response_class=ORJSONResponse,
            responses={200: {"model": ResponseModel}, 400: {"model": ResponseModel}, 422: {"model": ResponseModel}})
@inject
async def activate(email: EmailStr, code: str, service: FromDishka[UserUseCases]):
    dto = UserOTP(email=email, otp=code)
    response = await service.activate_user(dto)
    if not response:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Code expired!")
    return ORJSONResponse(status_code=status.HTTP_200_OK, content={"message": "ok"})


@router.post('/login',
             status_code=status.HTTP_200_OK,
             tags=endpoints_tag,
             response_class=ORJSONResponse,
             responses={200: {"model": ResponseModel}, 422: {"model": ResponseModel}, 401: {"model": ResponseModel}})
@inject
async def login(email: Annotated[EmailStr, Form()], password: Annotated[str, Form()], service: FromDishka[UserUseCases]):
    data = UserLoginForm(email=email, password=password)
    response = await service.login(data)
    if not response:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid email or password")
    return ORJSONResponse(status_code=status.HTTP_200_OK, content={"message": "ok"})





