from fastapi import APIRouter
from faststream.rabbit.fastapi import RabbitRouter
from backend.api.api_v1.endpoints.users import router as user_router
from backend.api.api_v1.endpoints.clients import router as clients_router


api_router = APIRouter(prefix='/v1')
api_router.include_router(user_router)
api_router.include_router(clients_router)

broker_api = RabbitRouter()

